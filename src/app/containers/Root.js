import React from 'react';
import {Switch, Route } from "react-router-dom";
import { connect } from "react-redux";

import { Header } from '../components/Header';
import { Home } from "../components/Home";
import { Main } from "../components/Main";
import { User, Users, UserProfile } from "../components/User";

import {setName} from "../actions/userActions";

class Root extends React.Component {

  render(){
    return(

      <div className="container-fluid ">

          <div className="row">
              <div className="col-xs-10 col-xs-offset-1">
                  <Header/>
              </div>
          </div>
          <div className="row">
              <div className="col-xs-10 col-xs-offset-1">
                  <Main changeUsername={() => this.props.setName("overmars")}/>
              </div>
          </div>

          <div className="row">
              <div className="col-xs-10 col-xs-offset-1">
                  <UserProfile username={this.props.user.name}/>
              </div>
          </div>

          <div className="row">
              <div className="col-xs-10 col-xs-offset-1 jumbotron">

                <Switch>
                  <Route exact path='/' component={Home}/>
                  <Route path='/home' component={Home}/>
                  <Route path='/main'>
                    <Main changeUsername={() => this.props.setName("overmars")} />
                  </Route>
                  <Route path='/users' component={Users}/>
                  <Route path='/user/:user_id' component={User}/>
                  <Route path='/userprofile/' >
                    <UserProfile username={this.props.user.name}/>
                  </Route>
                </Switch>

              </div>
          </div>

      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    user: state.user,
    math: state.math
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    setName: (name) => {
      dispatch(setName(name));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Root);
