import React from "react";

export const Main = (props ) => {


    return(
        <div>
            <h3>Main</h3>
            <button onClick={props.changeUsername.bind(this)}className="btn btn-primary">
              Change username
            </button>
        </div>
    );
}
