import React from "react";
import {Link } from "react-router-dom";

export const Header = (props) => {
   return(
       <nav className="navbar navbar-default  ">
           <div className="container-fluid">
               <div className="navbar-header">
                  <ul className="nav navbar-nav">
                      <li><Link to={"/home"}> Home </Link></li>
                      <li><Link to={"/main"}> Main </Link></li>
                      <li><Link to={"/users"}> Users </Link></li>
                      <li><Link to={"/user/2"}> Individual user </Link></li>
                  </ul>
              </div>
           </div>
       </nav>
   );
};
