import React from "react";
import { render } from "react-dom";
import { BrowserRouter } from "react-router-dom";

import  Root  from "./Root";

export class App extends React.Component{

    render(){
      return(
          <div>
            <BrowserRouter>
              <Root/>
            </BrowserRouter>
          </div>
      );
    }
}
