import React from 'react';

import {Link } from "react-router-dom";



export const Users = (props) => {
      return(
          <div>
              <h3> Many Users Page</h3>
              <Link to={"/home"} className="btn btn-primary" > Go home </Link>
          </div>
      );

}

export const User = (props) => {
      return(
          <div>
              <h3> Individual user Page</h3>
              <p> ID : {props.match.params.user_id} </p>

              <Link to={"/users"} className="btn btn-warning" > Go to users </Link>
          </div>
      );
}

export const UserProfile = (props) => {
      return(
          <div>
              <h3> Individual user Page</h3>
              <p> ID : {props.username} </p>

              <Link to={"/users"} className="btn btn-warning" > Go to users </Link>
          </div>
      );
}
